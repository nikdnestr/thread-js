import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';
import AddPost from '../../thread/components/add-post/add-post';
import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  onPostDeletion,
  onPostEdit,
  userId
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [postEdit, setPostEdit] = React.useState(false);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostDeletion = () => onPostDeletion(id);
  const handlePostEdit = text => {
    onPostEdit(id, text);
    setPostEdit(false);
  };

  let object;

  if (postEdit) {
    object = <AddPost onPostAdd={handlePostEdit} text={post} buttonText="Edit" />;
  } else {
    object = (
      <Card style={{ width: '100%' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta>
            <span className="date">
              {' '}
              posted by&nbsp;
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          <Card.Description>{body}</Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostLike}
          >
            <Icon name={IconName.THUMBS_UP} />
            {likeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostDislike}
          >
            <Icon name={IconName.THUMBS_DOWN} />
            {dislikeCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handleExpandedPostToggle}
          >
            <Icon name={IconName.COMMENT} />
            {commentCount}
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => sharePost(id)}
          >
            <Icon name={IconName.SHARE_ALTERNATE} />
          </Label>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => setPostEdit(true)}
          >
            <Icon name={IconName.EDIT} />
          </Label>
          {post.userId === userId && (
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handlePostDeletion}
            >
              <Icon name={IconName.DELETE} />
            </Label>
          )}
        </Card.Content>
      </Card>
    );
  }

  return object;
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  onPostDeletion: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired
};

export default Post;

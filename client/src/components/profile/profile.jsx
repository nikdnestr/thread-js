import * as React from 'react';
import { useSelector } from 'react-redux';
import { Grid, Image, Input, Form, Label } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [body, setBody] = React.useState('');
  const [status, setStatus] = React.useState('');
  const [editStatus, setEditStatus] = React.useState(false);

  const handleStatusEdit = text => {
    setEditStatus(false);
    setStatus(text);
  };

  let statusObject;

  if (editStatus) {
    statusObject = (
      <Form onSubmit={() => handleStatusEdit(body)}>
        <Input
          name="body"
          value={body}
          placeholder="Enter here"
          onChange={ev => setBody(ev.target.value)}
        />
      </Form>
    );
  }

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image
          centered
          src={user.image?.link ?? DEFAULT_USER_AVATAR}
          size="medium"
          circular
        />
        <div style={{ margin: '20px' }}>
          <h2>{status}</h2>
          {statusObject}
          <Label
            style={{ marginTop: '20px' }}
            onClick={() => setEditStatus(true)}
          >
            Change status
          </Label>
        </div>
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
